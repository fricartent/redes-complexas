package redes;

import java.util.ArrayList;
import java.util.Random;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.Graph;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.UndirectedGraph;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.ContainerFactory;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;

import difusão.LTM;

import util.Exportar;

public class SemEscala {

	private GraphModel graphModel;
	private UndirectedGraph graph;
	private Workspace workspace;
	/*
	 * Método para a geração de um modelo de rede sem escala. O primeiro parâmetro é para a representação do tamanho do grafo, 
	 * e o segundo representará uma medida para o tamanho do torneio.
	 */
	public void gerarModeloSemEscala(int tamGrafo, double porcentagem){

		ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
		pc.newProject();
		workspace = pc.getCurrentWorkspace();

		Container container = Lookup.getDefault().lookup(ContainerFactory.class).newContainer();
		/*RandomGraph grafoAleatorio = new RandomGraph();
		grafoAleatorio.setNumberOfNodes(200);
		grafoAleatorio.setWiringProbability(0.02);*/

		GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
		graphModel = graphController.getModel();
		graph = graphModel.getUndirectedGraph();

		graph = gerarGrafoInicial(graphModel, graph);
		
		int cont = 1;
		while(cont <= tamGrafo){
			
			Node novo = graphModel.factory().newNode();
			Node recuperado = selecaoTorneio(graphModel, graph, porcentagem);
			
			Edge link = graphModel.factory().newEdge(recuperado, novo, 1f, false);
			
			graph.addNode(novo);
			graph.addEdge(link);
			
			cont++;
		}
		System.out.println();
		/*graph.generate(container.getLoader());
		
		ImportController importController = Lookup.getDefault().lookup(ImportController.class);
		importController.process(container, new DefaultProcessor(), workspace);*/
		
		Exportar export = new Exportar();
		export.exportarGrafo(workspace, "sem_escala.gexf");
	}
	
	public void difusaoLTM(int quantNosAtivos, double limiar){
		
		for(Node it : graph.getNodes()){
			it.getNodeData().setLabel("vazio");
		}
		
		LTM difusao = new LTM(graphModel, graph, workspace);
		difusao.iniciarDifusao(quantNosAtivos, limiar);
		difusao.export();
	}

	/*
	 * Método para a criação de um gráfo inicial para a criação de um modelo de rede sem escala
	 */
	private UndirectedGraph gerarGrafoInicial(GraphModel graphModel, UndirectedGraph grafo) {
		
		ArrayList<Node> listaAuxiliar = new ArrayList<Node>();
		//Criando apenas os nós iniciais doo grafo
		for(int i = 0; i < 20; i++){
			Node n = graphModel.factory().newNode();
			grafo.addNode(n);
			listaAuxiliar.add(n);
		}

		Random random = new Random();
		//Gerar arestas entre os nós dos grafo inicial com uma fator de aleatoriedade
		for(int i = 0; i < 20; i++){
			
			int num = random.nextInt(20);
			
			if(i != num){
				Edge e = graphModel.factory().newEdge(listaAuxiliar.get(i), listaAuxiliar.get(num), 1f, false);
				grafo.addEdge(e);
				System.out.println();
			}else{
				int aux = 0;
				do{
					aux = random.nextInt(20);
					Edge e = graphModel.factory().newEdge(listaAuxiliar.get(i), listaAuxiliar.get(aux), 1f, false);
					grafo.addEdge(e);
				}while(aux != num);
					
			}
			
		}

		return grafo;
	}

	private Node selecaoTorneio(GraphModel graphModel, Graph grafoSemEscala, double porcentagem){
		
		Random random = new Random();
		int tamTotal = grafoSemEscala.getNodes().toArray().length;
		int randomTorneio = 0;
		int tamIntervalo = 0;
		//Node no = graphModel.factory().newNode();
		/*
		while(randomTorneio + tamIntervalo > tamTotal){
			randomTorneio = random.nextInt(tamTotal);
		}
		
		do{
			randomTorneio = random.nextInt(tamTotal);
			tamIntervalo = (int) (tamTotal - (tamTotal * porcentagem));
			tamIntervalo = tamTotal - tamIntervalo;
		}while(randomTorneio < 1 && (randomTorneio + tamIntervalo > tamTotal));
		*/
		for(;;){
			
			randomTorneio = random.nextInt(tamTotal);
			tamIntervalo = (int) (tamTotal - (tamTotal * porcentagem));
			tamIntervalo = tamTotal - tamIntervalo;
			
			if(randomTorneio > 1 && randomTorneio + tamIntervalo < tamTotal){
				break;
			}
		}
		
		int maiorGrau = 0;
		int posMaiorGrau = 0;
		
		for(int i = randomTorneio; i <= randomTorneio + tamIntervalo; i++){
			
			Node n = grafoSemEscala.getNode(i);
			if(n == null){
				System.out.println();
			}
			int grauAux = grafoSemEscala.getDegree(n);
			
			if(grauAux > maiorGrau){
				maiorGrau = grauAux;
				posMaiorGrau = i;
			}
		}
		
		return grafoSemEscala.getNode(posMaiorGrau);
	}

}
