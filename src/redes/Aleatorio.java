package redes;

import java.util.ArrayList;
import java.util.Random;

import org.gephi.graph.api.Edge;
import org.gephi.graph.api.GraphController;
import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.UndirectedGraph;
import org.gephi.io.generator.plugin.RandomGraph;
import org.gephi.io.importer.api.Container;
import org.gephi.io.importer.api.ContainerFactory;
import org.gephi.io.importer.api.ImportController;
import org.gephi.io.processor.plugin.DefaultProcessor;
import org.gephi.project.api.ProjectController;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;

import difusão.LTM;

import util.Exportar;

public class Aleatorio {

	private GraphModel graphModel;
	private UndirectedGraph graph;
	private Workspace workspace;

	/*
	 * Método gerador de rede baseado no modelo aleatório. Método utiliza a API do Gephi.
	 */
	public void gerarModeloAleatorio(){

		ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
		pc.newProject();
		Workspace workspace = pc.getCurrentWorkspace();

		Container container = Lookup.getDefault().lookup(ContainerFactory.class).newContainer();
		RandomGraph grafoAleatorio = new RandomGraph();
		grafoAleatorio.setNumberOfNodes(10000);
		grafoAleatorio.setWiringProbability(0.02);

		grafoAleatorio.generate(container.getLoader());

		ImportController importController = Lookup.getDefault().lookup(ImportController.class);
		importController.process(container, new DefaultProcessor(), workspace);

		Exportar export = new Exportar();
		export.exportarGrafo(workspace, "aleatorio.gexf");
	}

	/*
	 *  Geração da rede baseado no modelo aleatório. Recebe como parâmetros a quantidade de nós da rede,
	 *   e a porcentagem que será aplicada na criação dos link entre os nós
	 */
	public void gerarModeloAleatorio_v2(int quantNos, double porcentagem){

		ProjectController pc = Lookup.getDefault().lookup(ProjectController.class);
		pc.newProject();
		workspace = pc.getCurrentWorkspace();

		GraphController graphController = Lookup.getDefault().lookup(GraphController.class);
		graphModel = graphController.getModel();
		graph = graphModel.getUndirectedGraph();

		ArrayList<Node> listaNos = new ArrayList<Node>();

		for(int i = 0; i < quantNos; i++){
			Node n = graphModel.factory().newNode();
			listaNos.add(n);
			graph.addNode(n);
		}

		int cont = 0;
		Random random = new Random();

		while(cont < quantNos){

			int i = 0;
			int j = 0;

			do{
				i = random.nextInt(quantNos);
				j = random.nextInt(quantNos);
			}while( i == j);

			double per = Math.random();

			if(per <= porcentagem){
				Node n1 = listaNos.get(i);
				Node n2 = listaNos.get(j);
				Edge e = graphModel.factory().newEdge(n1, n2);
				
				//graph.addNode(n1);
				//graph.addNode(n2);
				graph.addEdge(e);
			}
			cont++;
		}

		Exportar export = new Exportar();
		export.exportarGrafo(workspace, "aleatorio.gexf");
	}

	public void difusaoLTM(int quantNosAtivos, double limiar){
		
		for(Node it : graph.getNodes()){
			it.getNodeData().setLabel("vazio");
		}
		//Node it = graph.getNode(10);
		LTM difusao = new LTM(graphModel, this.getGraph(), workspace);		
		difusao.iniciarDifusao(quantNosAtivos, limiar);
		difusao.export();
	}
	
	public GraphModel getGraphModel() {
		return graphModel;
	}

	public void setGraphModel(GraphModel graphModel) {
		this.graphModel = graphModel;
	}

	public UndirectedGraph getGraph() {
		return graph;
	}

	public void setGraph(UndirectedGraph graph) {
		this.graph = graph;
	}

}
