/*
Copyright 2008-2010 Gephi
Authors : Mathieu Bastian <mathieu.bastian@gephi.org>
Website : http://www.gephi.org

This file is part of Gephi.

Gephi is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Gephi is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Gephi.  If not, see <http://www.gnu.org/licenses/>.
 */
package samples;

import redes.Aleatorio;
import redes.SemEscala;

public class Main {

    public static void main(String[] args) {

    	//GenerateGraph g = new GenerateGraph();
    	//g.script();
    	//Aleatorio a = new Aleatorio();
    	//a.gerarModeloAleatorio();
    	
    	SemEscala semEscala = new SemEscala();
    	semEscala.gerarModeloSemEscala(9900, 0.20);
        /*HeadlessSimple headlessSimple = new HeadlessSimple();
        headlessSimple.script();
        
        WithAutoLayout autoLayout = new WithAutoLayout();
        autoLayout.script();

        ParallelWorkspace parallelWorkspace = new ParallelWorkspace();
        parallelWorkspace.script();

        PartitionGraph partitionGraph = new PartitionGraph();
        partitionGraph.script();

        RankingGraph rankingGraph = new RankingGraph();
        rankingGraph.script();

        Filtering filtering = new Filtering();
        filtering.script();

        ImportExport importExport = new ImportExport();
        importExport.script();

        MYSQLImportExport mYSQLImportExport = new MYSQLImportExport();
        mYSQLImportExport.script();

        ManualGraph manualGraph = new ManualGraph();
        manualGraph.script();

        ManipulateAttributes manipulateAttributes = new ManipulateAttributes();
        manipulateAttributes.script();

        DynamicMetric longitudinalGraph = new DynamicMetric();
        longitudinalGraph.script();

        ImportDynamic importDynamic = new ImportDynamic();
        importDynamic.script();*/
    }
}
