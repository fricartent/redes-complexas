package util;

import java.io.File;
import java.io.IOException;

import org.gephi.io.exporter.api.ExportController;
import org.gephi.io.exporter.spi.GraphExporter;
import org.gephi.project.api.Workspace;
import org.openide.util.Lookup;

public class Exportar {

	public void exportarGrafo(Workspace workspace, String fileName) {

		//Export full graph
		ExportController ec = Lookup.getDefault().lookup(ExportController.class);
		try {
			ec.exportFile(new File(fileName));
		} catch (IOException ex) {
			ex.printStackTrace();
			return;
		}

		//Export only visible graph
		GraphExporter exporter = (GraphExporter) ec.getExporter("gexf");     //Get GEXF exporter
		exporter.setExportVisible(true);  //Only exports the visible (filtered) graph
		exporter.setWorkspace(workspace);
		try {
			ec.exportFile(new File(fileName), exporter);
		} catch (IOException ex) {
			ex.printStackTrace();
			return;
		}


	}
}
