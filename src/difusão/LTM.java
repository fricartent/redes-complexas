package difusão;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import org.gephi.graph.api.GraphModel;
import org.gephi.graph.api.Node;
import org.gephi.graph.api.UndirectedGraph;
import org.gephi.project.api.Workspace;

import util.Exportar;

public class LTM {

	private Queue<Node> listaAtivos;
	private Queue<Node> listaVizinhos;

	private GraphModel graphModel;
	private UndirectedGraph graph;	
	private Workspace workspace;

	public LTM(GraphModel graphModel, UndirectedGraph graph, Workspace workspace) {

		listaAtivos = new LinkedList<Node>();
		listaVizinhos = new LinkedList<Node>();

		this.graph = graph;
		this.graphModel = graphModel;
		this.workspace = workspace;
	}

	//Ordenar a fronteira
	public void iniciarDifusao(int quantAtivar, double limiar){

		ativarNos(quantAtivar);

		/*for(Node n : graph.getNodes()){
			if(!listaAtivos.contains(n)){
				listaVizinhos.add(n);
			}
		}*/

		Queue<Node> fronteira = new LinkedList<Node>();
		Queue<Node> auxAtivos = new LinkedList<Node>();
		auxAtivos.addAll(listaAtivos);

		//início busca em largura
		//pega um nó ativo.
		Node ativo = auxAtivos.poll();
		//expansão da fronteira a partir do nó ativo.
		for(Node it : graph.getNeighbors(ativo)){
			/*if(!listaAtivos.contains(it)){
				fronteira.add(it);			
			}*/

			if(!it.getNodeData().getLabel().equals("ativado")){
				fronteira.add(it);
			}
		}

		while(!fronteira.isEmpty()){

			//Ordena os nós da fronteira a partir do grau
			if(fronteira.size() > 1){
				fronteira = ordenarNos(fronteira);
			}
			Node exp = null;

			/*do{

				exp = fronteira.peek();

			}while(!listaAtivos.contains(exp));*/

			for(;;){
				exp = fronteira.poll();
				/*if(!listaAtivos.contains(exp)){
					break;
				}
				 */
				if(!exp.getNodeData().getLabel().equals("ativado")){
					break;
				}
			}

			//cálculo da probabilidade de um dado nó ser ativado
			int numVizAtivos = 0;

			for(Node it : graph.getNeighbors(exp)){
				/*if(listaAtivos.contains(it)){
					numVizAtivos++;
				}*/
				if(it.getNodeData().getLabel().equals("ativado")){
					numVizAtivos++;
				}
			}

			double grauNo = graph.getDegree(exp);
			double probNoFronteira = numVizAtivos / grauNo;

			if(probNoFronteira > limiar){
				exp.getNodeData().setColor(0, 0, 1);
				exp.getNodeData().setLabel("ativado");
				listaAtivos.add(exp);
				auxAtivos.add(exp);
				//fronteira.remove(exp);
			}

			ativo = auxAtivos.poll();

			if(ativo != null){
				for(Node it : graph.getNeighbors(ativo)){
					/*if(!listaAtivos.contains(it)){
						fronteira.add(it);			
					}*/
					if(!it.getNodeData().getLabel().equals("ativado")){
						fronteira.add(it);
					}
				}
			}
		}

		export();
	}

	public void export(){

		for(Node i : graph.getNodes()){
			if(listaAtivos.contains(i)){

			}
		}
		Exportar export = new Exportar();
		export.exportarGrafo(workspace, "difusao.gexf");
	}

	private void ativarNos(int quantAtivar) {

		int it = 0;
		Random random = new Random();

		while(it < quantAtivar){

			int r = random.nextInt(graph.getNodes().toArray().length);
			Node n = graph.getNode(r);
					
			do{
				r = random.nextInt(graph.getNodes().toArray().length);
				n = graph.getNode(r);
				
				n.getNodeData().setColor(0, 0, 1);
				n.getNodeData().setLabel("ativado");
				listaAtivos.add(graph.getNode(r));
				
			}while(!n.getNodeData().getLabel().equals("ativado"));
			
			/*for(;;){
				int r = random.nextInt(graph.getNodes().toArray().length);
				Node n = graph.getNode(r);
			
				if(!n.getNodeData().getLabel().equals("ativado")){
					n.getNodeData().setColor(0, 0, 1);
					n.getNodeData().setLabel("ativado");
					listaAtivos.add(graph.getNode(r));
					break;
				}
			}
			*/
			/*
			if(!listaAtivos.contains(n) && n != null){
				n.getNodeData().setColor(0, 0, 1);
				listaAtivos.add(graph.getNode(r));
			}
			 */

			it++;
		}
		int i = 0;
		i = i+1;
	}

	/*
	 * Método para a ordenação de nós com base no grau de cada nó.
	 */
	private Queue<Node> ordenarNos(Queue<Node> fronteira){

		/*
		ArrayList<Node> listAux = new ArrayList<Node>();

		for(Node it : graph.getNodes()){
			listAux.add(it);
		}
		 */
		Object[] arrayNos = fronteira.toArray();

		for(int i = arrayNos.length - 1; i >= 1; i--){
			for(int j = 0; j < i; j++){
				int comp1 = graph.getDegree((Node) arrayNos[j]);
				int comp2 = graph.getDegree((Node) arrayNos[j+1]);
				if(comp1 < comp2){
					Node aux = (Node) arrayNos[j];
					arrayNos[j] = arrayNos[j+1];
					arrayNos[j+1] = aux;
				}
			}
		}

		//listAux.clear();
		fronteira.clear();
		for(Object iterator : arrayNos){
			fronteira.add((Node) iterator);
		}

		return fronteira;
	}

	public GraphModel getGraphModel() {
		return graphModel;
	}

	public void setGraphModel(GraphModel graphModel) {
		this.graphModel = graphModel;
	}

	public UndirectedGraph getGraph() {
		return graph;
	}

	public void setGraph(UndirectedGraph graph) {
		this.graph = graph;
	}
}
